from setuptools import setup

INSTALL_REQUIRES = [
    'kubernetes',
    'powerline-status',
]

# Lots of inspiration taken from https://github.com/so0k/powerline-kubernetes
# and https://github.com/zcmarine/powerkube
setup(
    name='powerline-kube-segment',
    version='0.0.1',
    description='A powerline segment for kubernetes contexts',
    author='Eric Timmons',
    author_email='nasafreak@gmail.com',
    packages=['powerline_kube_segment'],
    install_requires=INSTALL_REQUIRES,
)
