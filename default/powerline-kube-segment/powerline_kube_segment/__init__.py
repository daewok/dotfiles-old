from kubernetes import config
from powerline.theme import requires_segment_info

_KUBE_SYMBOL = u'\U00002638 '


@requires_segment_info
def context(pl, segment_info, show_context=True,
            show_namespace=True, alert_namespaces=()):
    pl.debug('Running powerline-kube-segment')

    render_context = segment_info['environ'].get('RENDER_POWERLINE_KUBERNETES',
                                                 'YES')
    if render_context != 'YES':
        return []

    if not any([show_context, show_namespace]):
        return []

    segments_list = []

    if show_context:
        segments_list.append(
            {'contents': config.list_kube_config_contexts()[1]['name'],
             'highlight_groups': ['kubernetes_context'],
             }
        )

    if show_namespace:
        namespace = config.list_kube_config_contexts()[1]['context']['namespace']
        be_alert = namespace in alert_namespaces
        if be_alert:
            highlight_group = 'kubernetes_namespace:alert'
        else:
            highlight_group = 'kubernetes_namespace'
        segments_list.append(
            {'contents': namespace,
             'highlight_groups': [highlight_group],
             }
        )

    # Add the Kubernetes symbol before the first segment
    first_highlight_group = segments_list[0]['highlight_groups']
    segments_list.insert(0, {'contents': _KUBE_SYMBOL,
                             'highlight_groups': first_highlight_group
                             }
                         )
    return segments_list
