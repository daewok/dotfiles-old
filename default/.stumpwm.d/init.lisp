;;; -*- Mode:Common-Lisp -*-
(in-package :stumpwm-user)

(when (string= "rocinante" (machine-instance))
  (add-to-load-path #p"/usr/share/common-lisp/source/stumpwm-contrib/modeline/battery-portable/")
  (load-module "battery-portable"))

(load-module "ttf-fonts")
(set-font (make-instance 'xft:font
                         :family "Hack"
                         :subfamily "Regular"
                         :size 10
                         :antialias t))
;; (ql:quickload :swank)
;; (swank-loader:init)
;; (swank:create-server :port 4004
;;                      :style swank:*communication-style*
;;                      :dont-close t)

(setf *screen-mode-line-format* "^[^2*<%d>^] %B [^B%n^b] %W")

;;(setf *screen-mode-line-format* "^[^2*<%d>^] [^B%n^b] %W")

(setf *window-format* "^[^7*%m%n%s^]%50t")
(setf *time-modeline-string* "%k:%M:%S")

;; input focus is transferred to the window you click on
(setf *mouse-focus-policy* :click)

(set-prefix-key (kbd "C-i"))

(defcommand urxvt () ()
            "Start a urxvt instance."
            (run-shell-command "urxvtc &"))

(defcommand st () ()
  "Start an st instance."
  (run-shell-command "st &"))

(defcommand st-with-tmux () ()
  "Start an st instance with tmux."
  (run-shell-command "st -e ~/.local/bin/smart-tmux &"))

(define-key *root-map* (kbd "c") "st-with-tmux")
(define-key *root-map* (kbd "C-c") "st")

(defcommand set-bg () ()
            "Set the background image."
            (run-shell-command "feh --recursive --randomize --bg-scale /usr/local/share/backgrounds/"))

(defcommand xlock () ()
            "Lock the X display."
            (run-shell-command "xscreensaver-command -lock"))

(defcommand emacs () ()
  "Start emacs unless it is already running, in which case focus it."
  (run-or-raise "emacs" '(:class "Emacs")))

(defcommand new-emacs () ()
  "Start emacs"
  (run-shell-command "emacs"))

(defcommand new-emacs-client () ()
            "Start an emacs client."
            (run-shell-command "emacsclient -a \"\" -c"))

(defcommand emacs-client () ()
            "Start an emacs client."
            (run-or-raise "emacsclient -a \"\" -c" '(:class "Emacs")))

(define-key *root-map* (kbd "e") "emacs-client")
(define-key *root-map* (kbd "E") "new-emacs-client")

(define-key *root-map* (kbd "C-e") "new-emacs")

(defcommand pamixer () ()
            "Launch a terminal with pulse-audio mixer."
            (run-shell-command "st -e pulsemixer"))

(defcommand ncmpcpp () ()
            "Launch a terminal with ncmpcpp."
            (run-shell-command "st -e ncmpcpp"))

(define-key *root-map* (kbd "v") "pamixer")
(define-key *root-map* (kbd "l") "xlock")

;;; Hibernate
(stumpwm::define-keysym #x1008ff1b "XF86Search")

(defcommand hibernate-ram () ()
            "go to sleep"
            (run-shell-command "sudo hibernate-ram"))

(define-key *top-map* (kbd "XF86Search") "hibernate-ram")

;;; Volume
(stumpwm::define-keysym #x1008ff11 "XF86AudioLowerVolume")
(stumpwm::define-keysym #x1008ff12 "XF86AudioMute")
(stumpwm::define-keysym #x1008ff13 "XF86AudioRaiseVolume")

(stumpwm::define-keysym #x1008ffb2 "XF86AudioMicMute")

;; MPD
(stumpwm::define-keysym #x1008ff16 "XF86AudioPrev")
(stumpwm::define-keysym #x1008ff14 "XF86AudioPlay")
(stumpwm::define-keysym #x1008ff17 "XF86AudioNext")

(stumpwm::define-keysym #x1008ff02 "XF86MonBrightnessUp")
(stumpwm::define-keysym #x1008ff03 "XF86MonBrightnessDown")

(defcommand amixer-master-1- () ()
            "decrease volume."
            (run-shell-command "amixer sset Master,0 1%-"))
(defcommand amixer-master-1+ () ()
            "increase volume."
            (run-shell-command "amixer sset Master,0 1%+"))
(defcommand amixer-master-toggle () ()
            "toggle volume."
            (run-shell-command "amixer sset Master,0 toggle"))

(defcommand amixer-mic-toggle () ()
            "toggle mic"
            (run-shell-command "amixer sset Capture,0 toggle"))

(define-key *top-map* (kbd "XF86AudioLowerVolume")   "amixer-master-1-")
(define-key *top-map* (kbd "XF86AudioRaiseVolume")   "amixer-master-1+")
(define-key *top-map* (kbd "XF86AudioMute")          "amixer-master-toggle")
(define-key *top-map* (kbd "XF86AudioMicMute")       "amixer-mic-toggle")

(defcommand backlight-up () ()
            "Increase backlight"
            (run-shell-command "xbacklight -inc 10"))

(defcommand backlight-down () ()
            "Increase backlight"
            (run-shell-command "xbacklight -dec 10"))

(define-key *top-map* (kbd "XF86MonBrightnessUp")    "backlight-up")
(define-key *top-map* (kbd "XF86MonBrightnessDown")  "backlight-down")

(defcommand scrot/all () ()
            "Take screenshot."
            (echo (run-shell-command "scrot '%F-%H-%M-%S_$wx$h_scrot.png' -e 'mv $f ~/shots/'" t)))

(defcommand scrot/window () ()
            "Take screenshot of currently active window."
            (echo (run-shell-command "scrot -u '%F-%H-%M-%S_$wx$h_scrot.png' -e 'mv $f ~/shots/'" t)))

(defcommand scrot/select () ()
            "Take screenshot of selected region."
            (echo (run-shell-command "scrot -s '%F-%H-%M-%S_$wx$h_scrot.png' -e 'mv $f ~/shots/'" t)))

(define-key *top-map* (kbd "Print") "scrot/all")
(define-key *top-map* (kbd "C-Print") "scrot/window")
(define-key *top-map* (kbd "M-Print") "scrot/select")

(defcommand mpc-play/pause () ()
            "Play or Pause MPD."
            (echo (run-shell-command "mpc toggle" t)))

(defcommand mpc-next () ()
            "Next song."
            (echo (run-shell-command "mpc next" t)))

(defcommand mpc-prev () ()
            "Previous song."
            (echo (run-shell-command "mpc prev" t)))

(defcommand mpc-status () ()
            "MPD status"
            (echo (run-shell-command "mpc status" t)))

(define-key *top-map* (kbd "XF86AudioPlay") "mpc-play/pause")
(define-key *top-map* (kbd "XF86AudioNext") "mpc-next")
(define-key *top-map* (kbd "XF86AudioPrev") "mpc-prev")

(defparameter *task-map* (make-sparse-keymap))

(define-key *root-map* (kbd "C-t") '*task-map*)

(defcommand task () ()
            "run task next."
            (echo (run-shell-command "task 2>&1" t)))

(define-key *root-map* (kbd "t") "task")
(define-key *task-map* (kbd "t") "task")

(defcommand task/add (task)
  ((:string "Task to add: "))
  (echo (run-shell-command (concatenate 'string "task add " task " 2>&1") t)))

(defcommand task/all (filter)
  ((:string "filter: "))
  (echo (run-shell-command (concatenate 'string "task " filter " all 2>&1") t)))

(defcommand task/done (filter mods)
  ((:string "Filter: ")
   (:string "Mods: "))
  (echo (run-shell-command (concatenate 'string "task " filter " done " mods " 2>&1") t)))

(defcommand task/list (filter)
  ((:string "Filter: "))
  (echo (run-shell-command (concatenate 'string "task " filter " list 2>&1") t)))

(defcommand task/sync ()
  ()
  (echo (run-shell-command "task sync 2>&1" t)))

(define-key *task-map* (kbd "a") "task/add")
(define-key *task-map* (kbd "d") "task/done")
(define-key *task-map* (kbd "l") "task/list")
(define-key *task-map* (kbd "M-l") "task/all")
(define-key *task-map* (kbd "s") "task/sync")

(defparameter *mpd-map* (make-sparse-keymap))

(define-key *root-map* (kbd "C-m") '*mpd-map*)

(define-key *mpd-map* (kbd "SPC") "mpc-play/pause")
(define-key *mpd-map* (kbd "p") "mpc-prev")
(define-key *mpd-map* (kbd "n") "mpc-next")
(define-key *mpd-map* (kbd "s") "mpc-status")
(define-key *mpd-map* (kbd "C-SPC") "mpc-play/pause")
(define-key *mpd-map* (kbd "C-p") "mpc-prev")
(define-key *mpd-map* (kbd "C-s") "mpc-status")
(define-key *mpd-map* (kbd "l") "ncmpcpp")

(defvar *mpd-minor-mode-active* nil)
(defcommand mpd-minor-mode () ()
  (unless *mpd-minor-mode-active*
    (stumpwm::push-top-map *mpd-map*)
    (setf *mpd-minor-mode-active* t)))

(defcommand stop-mpd-minor-mode () ()
  (when *mpd-minor-mode-active*
    (stumpwm::pop-top-map)
    (setf *mpd-minor-mode-active* nil)))

(define-key *mpd-map* (kbd "m") "mpd-minor-mode")
(define-key *mpd-map* (kbd "C-m") "mpd-minor-mode")

(define-key *mpd-map* (kbd "C-g") "stop-mpd-minor-mode")
