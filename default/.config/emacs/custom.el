(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes '(sanityinc-tomorrow-eighties))
 '(custom-safe-themes
   '("628278136f88aa1a151bb2d6c8a86bf2b7631fbea5f0f76cba2a0079cd910f7d" default))
 '(epg-gpg-program "qubes-gpg-client-wrapper")
 '(notmuch-saved-searches
   '((:name "mit-inbox" :query "tag:inbox AND tag:accounts/etimmons@mit.edu AND date:2-weeks..")
     (:name "nasafreak-inbox" :query "tag:inbox AND tag:accounts/nasafreak@gmail.com")
     (:name "inbox" :query "tag:inbox" :key "i")
     (:name "unread" :query "tag:unread" :key "u")
     (:name "flagged" :query "tag:flagged" :key "f")
     (:name "sent" :query "tag:sent" :key "t")
     (:name "drafts" :query "tag:draft" :key "d")
     (:name "all mail" :query "*" :key "a")) t)
 '(safe-local-variable-values
   '((whitespace-style quote
                       (face trailing empty tabs))
     (whitespace-action)
     (eval add-hook 'before-save-hook 'copyright-update nil t)
     (indent-tabs-mode nil)
     (flycheck-disabled-checkers
      '(emacs-lisp emacs-lisp-checkdoc))
     (eval when
           (and
            (buffer-file-name)
            (file-regular-p
             (buffer-file-name))
            (string-match-p "^[^.]"
                            (buffer-file-name)))
           (emacs-lisp-mode)
           (when
               (fboundp 'flycheck-mode)
             (flycheck-mode -1))
           (unless
               (featurep 'package-build)
             (let
                 ((load-path
                   (cons ".." load-path)))
               (require 'package-build)))
           (package-build-minor-mode)
           (set
            (make-local-variable 'package-build-working-dir)
            (expand-file-name "../working/"))
           (set
            (make-local-variable 'package-build-archive-dir)
            (expand-file-name "../packages/"))
           (set
            (make-local-variable 'package-build-recipes-dir)
            default-directory))))
 '(sly-lisp-indent-maximum-backtracking 20))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(fill-column-indicator ((t (:foreground "dark gray")))))
